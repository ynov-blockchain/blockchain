import http.client
import json
import pandas as pd  

  
  
connection = http.client.HTTPConnection('api.football-data.org')
headers = { 'X-Auth-Token': 'd3f7862c15044231bbc2c8a7df2f6054' }
connection.request('GET', '/v2/competitions/', None, headers )
response = json.loads(connection.getresponse().read().decode())
print(response)
df = pd.json_normalize(response, 'competitions')
pd.set_option('display.max_columns', None)
print(df.head(100))
# df=df[["id","utcDate","status","competition.id","score.winner","score.fullTime.homeTeam","score.fullTime.awayTeam", \
#        "homeTeam.id","homeTeam.name","awayTeam.id","awayTeam.name"]]
# print(df.head(100))
df=df[["id","name"]]
print(df.head(100))
fichier = open("insert_data_competition.sql", "w",encoding="utf-8")
fichier.write("use blockchain;\n")
fichier.write("delete table competitions;\n")

for i in range(len(df.index)):
    reference = (df.loc[i, 'id'], df.loc[i, 'name'])
    test="INSERT INTO competitions (id_competition, lib_competition) VALUES("+str(df.loc[i, 'id'])+","+'"'+str(df.loc[i, 'name'])+'");\n'
    print(test)
    fichier.write(test)
fichier.close()
