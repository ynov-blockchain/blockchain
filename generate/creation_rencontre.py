import http.client
import json
import pandas as pd  

def fonction_status():
    status=False
    if(str(df.loc[i, 'status']))=="FINISHED":
        status=True
    return status

def fonction_resultat():
    resultat="NULL"
    if(str(df.loc[i, 'score.winner']))=="AWAY_TEAM":
        resultat="2"
    elif((str(df.loc[i, 'score.winner'])))=="HOME_TEAM":
        resultat="1"
    elif((str(df.loc[i, 'score.winner'])))=="DRAW":
        resultat="0"
    return resultat

def fonction_score():
    score_domicile,score_exterieur="",""
    print(str(df.loc[i, 'score.fullTime.homeTeam']))
    if(str(df.loc[i, 'score.fullTime.homeTeam']))!="nan":
        score_domicile=str(int(df.loc[i, 'score.fullTime.homeTeam']))
        score_exterieur=str(int(df.loc[i, 'score.fullTime.awayTeam']))
    else:
        score_domicile="NULL"
        score_exterieur="NULL"
    return(str(score_domicile),str(score_exterieur))

def fonction_date():
    date_match=str(df.loc[i, 'utcDate']).replace("T", " ")
    date_match=date_match.replace("Z", "")
    return(date_match)

connection = http.client.HTTPConnection('api.football-data.org')
headers = { 'X-Auth-Token': 'd3f7862c15044231bbc2c8a7df2f6054' }
connection.request('GET', '/v2/competitions/2015/matches/', None, headers )
response = json.loads(connection.getresponse().read().decode())
print(response)
df = pd.json_normalize(response, 'matches')
# pd.set_option('display.max_columns', None)
print(df.head(100))
df=df[["id","utcDate","status","score.winner","score.fullTime.homeTeam","score.fullTime.awayTeam", \
        "homeTeam.id","awayTeam.id"]]
print(df.head(100))
fichier = open("insert_data_match.sql", "w",encoding="utf-8")
fichier.write("use blockchain;\n")
fichier.write("delete table matchs;\n")

for i in range(len(df.index)):
    resultat=fonction_resultat()
    status=fonction_status()
    score_domicile,score_exterieur=fonction_score()
    date_match=fonction_date()
    
    test="INSERT INTO matchs (id_match, id_team_home , id_team_away ,date_match, score_home,score_away,statut,result) VALUES("+str(df.loc[i, 'id'])+','+str(df.loc[i, 'homeTeam.id'])+','+str(df.loc[i, 'awayTeam.id'])+',"'+ str(date_match)+'",'+str(score_domicile)+','+str(score_exterieur)+','+str(status)+','+str(resultat)+');\n'
    print(test)
    fichier.write(test)
fichier.close()
