<?php namespace App\Controllers;

use App\Models\UserModel;

class Wallet extends BaseController
{
	public function index()
	{
		$model = new UserModel();
        $data['user'] = $model->where('id', session()->get('id'))->first();
		$data['transactions'] = $model->getTransactions($data['user']['id']);

		echo view('templates/header', $data);
		echo view('templates/nav');
		echo view('wallet');
		echo view('templates/footer');
	}
}
