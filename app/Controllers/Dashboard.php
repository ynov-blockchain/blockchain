<?php namespace App\Controllers;

use App\Models\MatchModel;
use App\Models\TeamModel;

class Dashboard extends BaseController
{
    public function index()
    {
        $model = new MatchModel();
        $matchs = $model->getList();
        $data['matchs'] = $matchs;

		echo view('templates/header', $data);
		echo view('templates/nav');
		echo view('dashboard');
		echo view('templates/footer');
	}

	public function viewmatch()
	{
		$model = new MatchModel();
		$matchs = $model->getMatch($_GET['id']);
		$data['matchs'] = $matchs;

		echo view('templates/header', $data);
		echo view('templates/nav');
		echo view('match');
		echo view('templates/footer');
	}
}
