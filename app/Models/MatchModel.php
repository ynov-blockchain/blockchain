<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\Query;

class MatchModel extends Model
{
    protected $table = 'matchs';
    protected $primaryKey ="id_match";
    protected $allowedFields = ['id_match', 'id_team_home', 'id_team_away', 'date_match', 'score_home', 'score_away','statut','result'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];
    protected $useAutoIncrement = true;

    public function __construct(){
        parent::__construct();
        $this->db=\Config\Database::Connect();
        $this->builder=$this->db->table('matchs');
    }

    protected function beforeInsert(array $data){
        $data['data']['created_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function beforeUpdate(array $data){
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }
    
    public function getList(){
        $sql = "SELECT t_h.lib_team as lib_home, t_a.lib_team as lib_away,date_match,score_home,score_away,statut,result,id_match 
                FROM matchs m 
                LEFT JOIN teams t_h ON m.id_team_home = t_h.id_team_home 
                LEFT JOIN teams t_a ON t_a.id_team_away=m.id_team_away 
                WHERE statut = 1";
        $query=$this->db->query($sql); 
        return $query->getResult();
    }

    public function getMatch($id){
        $sql = "SELECT t_h.lib_team as lib_home, t_a.lib_team as lib_away,date_match,score_home,score_away,statut,result,id_match 
                FROM matchs m 
                LEFT JOIN teams t_h ON m.id_team_home = t_h.id_team_home 
                LEFT JOIN teams t_a ON t_a.id_team_away=m.id_team_away 
                WHERE id_match = $id";
        $query=$this->db->query($sql); 
        return $query->getResult();
    }
}
