<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\Query;

class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey ="id";
    protected $allowedFields = ['firstname', 'lastname', 'email', 'password', 'solde', 'id_role', 'wallet', 'updated_at'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    public function __construct(){
        parent::__construct();
        $this->db=\Config\Database::Connect();
        $this->builder=$this->db->table('users');
    }

    protected function beforeInsert(array $data){
        $data = $this->passwordHash($data);
        $data['data']['created_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function beforeUpdate(array $data){
        $data = $this->passwordHash($data);
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function passwordHash(array $data){
        if(isset($data['data']['password']))
        $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);
        return $data;
    }

    public function getTransactions(int $id){
        $sql = "SELECT u_t.*, m.*, t_h.lib_team as lib_home, t_a.lib_team as lib_away
                FROM user_transaction u_t
                LEFT JOIN users u ON u.id = u_t.id_user
                LEFT JOIN matchs m ON u_t.id_match = m.id_match
                LEFT JOIN teams t_h ON m.id_team_home = t_h.id_team_home 
                LEFT JOIN teams t_a ON  m.id_team_away = t_a.id_team_away
                WHERE u.id = $id;";
        $query=$this->db->query($sql); 
        return $query->getResult();
    }
}
