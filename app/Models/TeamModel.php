<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\Query;

class MatchModel extends Model 
{
    protected $table = 'teams';
    protected $primaryKey ="id_team_home";
    protected $allowedFields = ['id_team_home', 'lib_team', 'id_competition' ];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];
    protected $useAutoIncrement = true;

    public function __construct(){
        parent::__construct();
        $this->db=\Config\Database::Connect();
        $this->builder=$this->db->table('teams');
    }

    protected function beforeInsert(array $data){
        $data['data']['created_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function beforeUpdate(array $data){
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }
}
