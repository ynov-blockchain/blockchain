<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\Query;

class CompetitionModel extends Model
{
    protected $table = 'competitions';
    protected $primaryKey ="id_competition";
    protected $allowedFields = ['lib_competition', 'updated_at'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    public function __construct(){
        parent::__construct();
        $this->db=\Config\Database::Connect();
        $this->builder=$this->db->table('competitions');
    }

    protected function beforeInsert(array $data){
        $data['data']['created_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function beforeUpdate(array $data){
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }
}
