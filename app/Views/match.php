<main role="main" class="col-md-10 offset-md-1 pt-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
            <h1 class="h1">Dashboard</h1>
          </div>
          <div class="table-responsive">
            <table id="table_match" class="table table-striped table-sm display">
              <thead>
                <tr>         
                  <th>Date</th>                  
                  <th>Equipe domicile</th>
                  <th>Score</th>
                  <th>Score extérieur</th>
                  <th>Choix</th>
                  <th>Mise</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              
              <?php foreach($matchs as $match): ?>

                    <tr>
                        <td><?= $match->date_match; ?></td>
                        <td><?= $match->lib_home; ?></td>
                        <td><?= $match->score_home; ?> - <?= $match->score_away; ?></td>
                        <td><?= $match->lib_away; ?></td>
                        <td><select name="choice" id="choice-select">
                        <option value="home">1</option>
                        <option value="draw">X</option>
                        <option value="away">2</option>
                        </select></td>
                        <td><input type="number" id="mise" name="mise"></input></td>
                        <td><input type="submit" value="Valider le pari"></input></td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>
    <script>
      $(document).ready( function () {
          $('#table_match').DataTable();
      } );
      </script>

