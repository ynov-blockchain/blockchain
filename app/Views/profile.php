<main role="main" class="col-md-10 offset-md-1 pt-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-4">
        <h1 class="h1"><?= $user['firstname'] . ' ' . $user['lastname'] ?></h1>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="row mb-30">
                <div class="col-sm-6 ">
                    <div class="box bg-dark">
                        <h3></h3>
                        <p class="value"></p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box bg-dark">
                        <h3></h3>
                        <p class="value"></p>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-6">
                    <div class="box bg-dark">
                        <h3></h3>
                        <p class="value"></p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <a class="text-decoration-none" href="/logout">
                        <div class="box bg-secondary">
                            <img alt="Déconnexion" class="mt-2" src="<?php echo base_url('/assets/images/log-out.png'); ?>" />
                            <p class="unit mt-3">Déconnexion</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title mb-4">Vos informations</h3>
                    <form class="" action="/profile" method="post">
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label text-right" for="firstname">Prénom</label>
                                <input type="text" class="form-control col-md-10 col-sm-12" name="firstname" id="firstname"
                                    value="<?= set_value('firstname', $user['firstname']) ?>">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label text-right" for="lastname">Nom</label>
                                <input type="text" class="form-control col-md-10 col-sm-12" name="lastname" id="lastname"
                                    value="<?= set_value('lastname', $user['lastname']) ?>">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-md-2 col-sm-12 col-form-label text-right" for="email">Adresse mail</label>
                                <input type="text" class="form-control col-md-10 col-sm-12" readonly id="email" value="<?= $user['email'] ?>">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="form-group col-md-6 col-sm-12 ">
                                    <div class="row">
                                        <label class="col-md-4 col-sm-12 col-form-label text-right" for="password">Mot de passe</label>
                                        <input type="password" class="form-control col-md-7 col-sm-12" name="password" id="password" value="">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 col-sm-12 ">
                                    <div class="row">
                                        <label class="col-md-5 col-sm-12 col-form-label text-right" for="password_confirm">Confirmer mot de passe</label>
                                        <input type="password" class="form-control col-md-7 col-sm-12" name="password_confirm" id="password_confirm" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($validation)): ?>
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= $validation->listErrors() ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Mettre à jour</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
