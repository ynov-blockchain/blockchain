<main role="main" class="col-md-10 offset-md-1 pt-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-4">
        <h1 class="h1">Grille des paris</h1>
    </div>
    <div class="table-responsive">
        <table id="table_match" data-page-length="6" class="table table-striped table-vcenter">
            <thead class="thead-dark">
                <tr> 
                <th></th>        
                <th>Rencontres</th>     
                <th class="text-center">1</th>
                <th class="text-center">X</th>
                <th class="text-center">2</th>
                <th></th>
                </tr>
            </thead>
            <tbody>  
                <?php foreach($matchs as $match): ?>
                <tr>
                    <td class="align-middle text-center">
                        <p class="mb-0 mt-1"><i class="fa fa-futbol-o fa-lg" aria-hidden="true"></i></p>
                        <small>Football, Ligne 1</small>
                    </td>
                    <td class="align-middle">
                        <a class="text-dark text-decoration-none"href="/dashboard/viewmatch?id=<?= $match->id_match; ?>">
                        <small><?= $match->date_match; ?></small>
                        <p class="mb-0"><b><?= $match->lib_home; ?> - <?= $match->lib_away; ?></b></p>
                        </a>
                    </td>
                    <td class="align-middle text-center">2.66</td>
                    <td class="align-middle text-center">4.36</td>
                    <td class="align-middle text-center">9.99</td>
                    <td class="align-middle text-center"><a class="text-dark" href="/dashboard/viewmatch?id=<?= $match->id_match; ?>"><i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i></a></td>
                    
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</main>
<script>
$(document).ready( function () {
    $('#table_match').DataTable({
        searching : false,
        bPaginate : true,
        bLengthChange : false,
        bFilter : true,
        bSort : false,
        bInfo : false,
        columns: [
            { "width": "10%" },
            { "width": "36%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "9%" }
        ]
    });
});
</script>


