<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/assets/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
    <title></title>
  </head>
  <body>
  <nav class="navbar sticky-top p-1 bg-primary">
      <ul class="navbar-nav px-3 ml-auto">
          <li class="nav-item white">
              <a class="nav-link text-white" href="/profile">
                <span data-feather="logout"><i class="fa fa-bell" aria-hidden="true"></i></span>
              </a>
          </li>
      </ul>
  </nav>

