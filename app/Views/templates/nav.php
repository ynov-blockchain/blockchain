<nav class="sidebar shadow-5 bg-dark">
  <div class="sidebar-sticky">
  
    <ul class="nav flex-column align-items-center leftnavbar">
    <li class="nav-item">
        <a class="nav-link" href="/">
          <span data-feather="dashboard"></span>
          <img class="logo" src="<?php echo base_url('/assets/images/logo.png'); ?>" />
          <p class="nav-title mt-3 text-white font-weight-bold">CRYNOV</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="/dashboard">
          <span data-feather="dashboard"></span>
          <img alt="Liste des paris" src="<?php echo base_url('/assets/images/list.png'); ?>" />
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/wallet">
          <span data-feather="wallet"></span>
          <img alt="Portefeuilles" src="<?php echo base_url('/assets/images/wallet.png'); ?>" />
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/profile">
          <span data-feather="profile"></span>
          <img alt="Mon profil" src="<?php echo base_url('/assets/images/user.png'); ?>" />
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/logout">
          <span data-featheru="logout"></span>
          <img alt="Déconnexion" src="<?php echo base_url('/assets/images/log-out.png'); ?>" />
        </a>
      </li>
    </ul>
  </div>
</nav>