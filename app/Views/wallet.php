<main role="main" class="col-md-10 offset-md-1 pt-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-4">
        <h1 class="h1">Portefeuille</h1>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="row mb-30">
                <div class="col-sm-6 ">
                    <div class="box bg-secondary">
                        <h3>Portefeuille</h3>
                        <p class="value"><?= $user['solde']; ?></p><p class="unit">CNY</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box bg-dark">
                        <h3>Statistique</h3>
                        <p class="value"></p><p class="unit">Par semaine</p>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-6">
                    <div class="box bg-dark">
                        <h3>Vistoire(s)</h3>
                        <p class="value"></p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box bg-dark">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <h3 class="card-title mb-4">Historique</h3>
            <table id="table_history" data-page-length="5" class="table table-striped table-vcenter table-sm">
                <thead class="thead-dark">
                <tr>
                    <th></th>
                    <th>Rencontre</th>
                    <th class="text-center">Pronostic</th>
                    <th class="text-center">Score</th>
                    <th class="text-center">Mise</th>
                    <th class="text-center">Gain</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($transactions as $transaction): ?>
                <?php //var_dump($transaction);die(); ?>
                    <tr>
                        <td class="align-middle text-center">
                            <p class="mb-0"><i class="fa fa-futbol-o" aria-hidden="true"></i></p>
                            <small>Football, Ligne 1</small>
                        </td>
                        <td class="align-middle ">
                            <small><?= $transaction->date_match; ?> </small>
                            <p class="mb-0 mt-small"><b><?= $transaction->lib_home; ?> - <?= $transaction->lib_away; ?></b></p>
                        </td>
                        <td class="align-middle text-center"><?= $transaction->choice; ?></td>
                        <td class="align-middle text-center"><?= $transaction->score_home; ?> - <?= $transaction->score_away; ?></td>
                        <td class="align-middle text-center"><?= $transaction->bet; ?></td>
                        <td class="align-middle text-center"></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <script>
    $(document).ready( function () {
        $('#table_history').DataTable({
            searching : false,
            bPaginate : true,
            bLengthChange : false,
            bFilter : true,
            bSort : false,
            bInfo : false,
            columns: [
                { "width": "15%" },
                { "width": "37%" },
                { "width": "12%" },
                { "width": "12%" },
                { "width": "12%" },
                { "width": "12%" }
            ]
        });
    } );
    </script>
</main>
