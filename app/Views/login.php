<div class="container">
    <div class="row login-page">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3  pt-3 pb-3 from-wrapper">
            <h3>Connexion</h3>
            <?php if (session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif; ?>
            <form class="" action="/" method="post">
                <div class="form-group">
                    <label for="email">Adresse mail</label>
                    <input type="text" class="form-control" name="email" id="email" value="<?= set_value('email') ?>">
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" class="form-control" name="password" id="password" value="">
                </div>
                <div class="row mt-4 mb-2">
                    <div class="col-12 col-sm-6">
                        <button type="submit" class="btn btn-primary">Connexion</button>
                    </div>
                    <div class="col-12 col-sm-6">
                        <a href="/register">Vous n'avez pas de compte?</a>
                    </div>
                </div>
            </form>
            <?php if (isset($validation)): ?>
                <div class="alert alert-danger" role="alert">
                    <?= $validation->listErrors() ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>