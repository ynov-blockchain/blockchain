-- Guide de démarrage Intégration

Lancer les commandes suivantes au niveau du fichier C:\wamp\www\blockchain\webpack.mix.js

- Run npm install
- npm run build
- npm run watch

http://crynov-dev/

Pour modifier le css du site il faut passer par : C:\wamp\www\blockchain\dev\scss_custom.scss

Si tu arrive pas à utiliser le pré processeur SCCS tu peux toujours utiliser le fichier pour faire du css "classique" : C:\wamp\www\blockchain\public\assets\css\style.css

-- Guide de démarrage dev

1.  Créer une base de données 'blockchain' et lancer le script :

    - create_table.sql (C:\wamp\www\blockchain\bdd)

    Éxécuter les scrits (C:\wamp\www\blockchain\bdd) pour avoir des données de test : - insert_data_competition.sql - insert_data_match.sql - insert_data_team.sql

2.  Vérifier .env le créer à partir de env

    Host = localhost
    user = root
    passeword =

    Activer debug uniquement en dev => ligne 17 dans le fichier .env CI_ENVIRONMENT = development

3.  Config WAMP v3.2.3 :

    - Php 7.4.9
    - Apache : 2.4.46
    - Mysql 5.7.31
    - ALLOW redirect (extension php)
    - create virtualhosthost blockchain/ qui pointe vers C:\wamp\www\blockchain\public

4.  Lancer la commande "composer install" pour créer le fichier vendor/ si soucis avec composer récupérer le vendor directement en .zip dans le channel "Documents" de Discord.

5.  Créer fichier writable et writable/cache au dessus de public

6.  Utilisation de Git les bonnes pratiques :

Avant de commencer de dev faire :

    Le commit si vous avez des modifications en cours :
    	- git add chemin
    	- git commit -m "Mon message de commit"

    Vérifier que vos branches sont à jour :
    	- git fetch Met à jour vos branches distantes local
    	- git merge --ff-only origin/votre_branche Met à jour votre_branche

Pour mettre en ligne votre travail il faut :

    revérifier que entre temps personne à commit du coup :
    	- git fetch Met à jour vos branches distantes local
    	- git merge --ff-only origin/votre_branche Met à jour votre_branche

    Si ça bloque (conflits) :
    	- git rebase

    Si fichier Both résoudre le conflit dans l'éditeur de fichier
    	- git rebase --continue  (peut être fait plusieurs fois si beaucoup de commit de retard)

    enfin faire :
    	- git push

    !!!! NE JAMAIS FAIRE DE PUSH DIRECTE SUR LA BRANCHE MASTER PASSER PAR LA BRANCHE PREPROD !!!!
    C'est la copie propre du site donc elle sert en cas de pépin.
    Elle est utilisée sur le serveur de production donc si tu push un erreur ça casse la prod.
    Donc tu push pas dessus :)

Commande utile pour savoir se qu'on fait :

    - git status => Liste les fichiers modifiés
    - git checkout branch => Permet de changer de branche
    - git checkout -b Permet de créer une nouvelle branche
    - git checkout fichier => Permet d'annuler les modifications des fichiers
    - git branch -av => liste les branches  et permet de voir si on est en avance ou en retard pas rapport à origin/votre_branche
    - git log => Historique des commits de la branche rajouter paramètre '--oneline' affichage plus claire
