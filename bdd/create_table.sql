CREATE DATABASE blockchain;

CREATE TABLE IF NOT EXISTS user_roles
(
    id_role INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    lib_role VARCHAR(100),
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,     
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP    
);

CREATE TABLE IF NOT EXISTS competitions
(
    id_competition INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    lib_competition VARCHAR(100),
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,     
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP    
);


CREATE TABLE IF NOT EXISTS teams
(
    id_team_home INT,
    id_team_away INT,
    lib_team VARCHAR(100),
    id_competition INT,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,     
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    PRIMARY KEY (id_team_home , id_team_away),   
    FOREIGN KEY (id_competition) REFERENCES competitions(id_competition)
);

CREATE TABLE IF NOT EXISTS matchs(
    id_match INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_team_home INT,
    id_team_away INT,
    date_match TIMESTAMP,
    score_home INT,
    score_away INT,
    statut BOOLEAN,
    result INT,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,     
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS users(     
id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,     
firstname VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',     
lastname VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',     
email VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',     
password VARCHAR(255) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', 
solde DOUBLE,
id_role INT,   
wallet VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',  
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,     
updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,     
FOREIGN KEY(id_role) REFERENCES user_roles(id_role) ) COLLATE='latin1_swedish_ci' ENGINE=MyISAM AUTO_INCREMENT=4 
;

CREATE TABLE IF NOT EXISTS user_transaction(
    id_trans INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    choice VARCHAR(1),
    bet INT,
    id_match INT,
    id_user INT,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,     
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
    FOREIGN KEY(id_match) REFERENCES matchs(id_match),
    FOREIGN KEY(id_user) REFERENCES users_test(id) 
);
	








