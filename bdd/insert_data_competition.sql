use blockchain;
delete table competitions;
INSERT INTO competitions (id_competition, lib_competition) VALUES(2006,"WC Qualification");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2023,"Primera B Nacional");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2024,"Superliga Argentina");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2025,"Supercopa Argentina");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2149,"Copa Liga Profesional");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2147,"WC Qualification");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2008,"A League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2026,"FFA Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2020,"Erste Liga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2012,"Bundesliga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2022,"Playoffs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2027,"ÖFB Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2028,"Coupe de Belgique");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2033,"Division 1B");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2009,"Jupiler Pro League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2032,"Playoffs II");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2010,"Supercoupe de Belgique");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2034,"LFPB");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2035,"Premier Liga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2037,"Copa do Brasil");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2038,"Série D");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2036,"Série C");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2029,"Série B");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2013,"Série A");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2039,"Kupa na Bulgarija");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2040,"A PFG");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2041,"Canadian Championship");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2042,"Playoffs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2043,"Supercopa de Chile");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2048,"Primera División");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2044,"Chinese Super League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2045,"Liga Postobón");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2046,"Superliga de Colombia");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2047,"Prva Liga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2049,"Synot Liga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2051,"DBU Pokalen");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2050,"Superliga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2141,"Play Offs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2052,"Copa Pilsener Serie A");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2139,"Football League Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2056,"FA Community Shield");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2055,"FA Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2053,"National League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2054,"League Two");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2030,"League One");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2016,"Championship");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2021,"Premier League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2057,"Meistriliiga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2146,"UEFA Europa League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2001,"UEFA Champions League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2018,"European Championship");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2058,"UEFA Women's EURO");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2007,"WC Qualification");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2059,"Suomen Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2031,"Veikkausliiga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2135,"Coupe de la Ligue");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2142,"Ligue 2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2015,"Ligue 1");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2144,"Playoffs 2/3");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2136,"Trophée des Champions");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2138,"Coupe de France");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2143,"Playoffs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2011,"DFB-Pokal");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2129,"Regionalliga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2140,"3. Liga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2004,"2. Bundesliga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2002,"Bundesliga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2133,"Frauen Bundesliga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2134,"DFL Super Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2131,"Greek Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2132,"Super League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2150,"Souper Kap Ellados");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2130,"Magyar Kupa");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2128,"NB I");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2127,"Úrvalsdeild");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2126,"I-League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2125,"Ligat ha'Al");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2124,"Supercoppa");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2122,"Coppa Italia");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2123,"Serie C");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2121,"Serie B");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2019,"Serie A");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2118,"J.League Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2117,"J. League Division 2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2119,"J. League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2120,"Super Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2116,"Virslīga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2115,"A Lyga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2114,"Premier League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2112,"SuperCopa MX");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2111,"Copa MX");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2113,"Liga MX");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2109,"KNVB Beker");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2005,"Jupiler League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2003,"Eredivisie");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2110,"Johan Cruijff Schaal");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2108,"League Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2107,"Premiership");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2106,"Tippeligaen");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2105,"1. divisjon");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2104,"Playoffs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2103,"WC Qualification");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2102,"Liga Panameña de Fútbol");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2101,"Primera División");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2099,"Superpuchar Polski");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2100,"Puchar Polski");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2098,"Taça de Portugal");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2096,"Liga2 Cabovisão");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2017,"Primeira Liga");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2097,"Supertaça Cândido de Oliveira");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2095,"Premier Division");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2092,"Liga II");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2094,"Liga I");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2093,"Supercupa României");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2089,"Russian Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2088,"FNL");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2137,"RFPL");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2090,"Playoffs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2091,"Russian Super Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2087,"Scottish Cup");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2085,"Championship");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2084,"Premier League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2086,"Playoffs 2/3");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2083,"ABSA Premiership");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2080,"Copa America");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2081,"Copa Sudamericana");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2082,"WC Qualification");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2079,"Copa del Rey");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2077,"Segunda División");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2014,"Primera Division");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2078,"Supercopa de España");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2074,"Superettan");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2073,"Allsvenskan");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2075,"Playoffs 2/3");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2076,"Playoffs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2071,"Schweizer Pokal");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2072,"Super League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2068,"1. Lig");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2070,"Süper Lig");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2069,"TFF Süper Kupa");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2065,"Kubok Ukrainy");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2064,"Premier Liha");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2066,"Playoffs 1/2");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2067,"Superkubok Ukrainy");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2145,"MLS");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2063,"Primera División");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2148,"Supercopa Uruguaya");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2062,"Primera División");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2061,"V-League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2060,"Welsh Premier League");
INSERT INTO competitions (id_competition, lib_competition) VALUES(2000,"FIFA World Cup");
